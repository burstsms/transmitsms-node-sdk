# Transmitsms NodeJS SDK #

for more info plase visit : http://www.burstsms.com.au/sms-products/sms-api

### Changelog ###

 * 1.0 - Initial release

### Installation ###

* $> npm install transmitsms-node-sdk

### Usage Example ###

```
var SMSSDK = require("transmitsms-node-sdk").transmitsms;

var SMS = new SMSSDK();

var message = {
    Authorization : 'Basic ' + new Buffer(config.get('key') + ':' + config.get('secret')).toString('base64'),
    message : content.message,
    to : item.MobilePhone.replace(/ /g,"")
};

SMS.send(message).then(function fulfill(result){
//logic handle succes
},function reject(result){
//logic to handle failure
});
```


