var SMS = require("../transmitsms.js").transmitsms;
describe('sms api', function() {
  it("should return AUTH_FAILED", function(done) {
    var msg = new SMS();
    var response = msg.send({
      "Authorization": "Basic MTVhZDI2NmM1MzhmYjM2YzRkOdfsgdfgfg6bW9vc2U=",
      "message": "sample",
      "to": "+639297700514"
    });
    response
      .catch(function(res) {
        expect(res.body.error.code).toEqual('AUTH_FAILED_NO_DATA');
        done();
      });

  });
  it("should return SUCCESS", function(done) {
    var msg = new SMS();
    var response = msg.send({
      "Authorization": "Basic MTVhZDI2NmM1MzhmYjM2YzRkOTBmMDEwNTVhZWY0OTQ6bW9vc2U=",
      "message": "sample",
      "to": "+639297700514"
    });
    response
      .then(function(res, error) {
        expect(res.body.message_id).not.toBe(null);
        done();
      });

  });
});