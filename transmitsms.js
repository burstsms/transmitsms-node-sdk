/*jshint -W069 */
/**
 * Our flexible and powerful suite of API's enable developers to add SMS send functionality to their applications. Our callback systems allow you to receive inbound messages and report on deliveries. You can also create lists, lease numbers, set up keywords and schedule messages. Our API servers are dedicated and do not share resources with our bulk messaging system so that your text messages and responses are managed quickly and reliably.
 * @class transmitsms
 * @param {(string|object)} [domainOrOptions] - The project domain or options object. If object, see the object's optional properties.
 * @param {string} [domainOrOptions.domain] - The project domain
 * @param {object} [domainOrOptions.token] - auth token - object with value property and optional headerOrQueryName and isQuery properties
 */
var transmitsms = (function() {
    'use strict';

    var request = require('request');
    var Q = require('q');

    function transmitsms(options) {
        var domain = (typeof options === 'object') ? options.domain : options;
        this.domain = domain ? domain : 'https://api.transmitsms.com';
        if (this.domain.length === 0) {
            throw new Error('Domain parameter must be specified as a string.');
        }
        this.token = (typeof options === 'object') ? (options.token ? options.token : {}) : {};
    }

    transmitsms.prototype.request = function(method, url, parameters, body, headers, queryParameters, form, deferred) {
        var req = {
            method: method,
            uri: url,
            qs: queryParameters,
            headers: headers,
            body: body
        };
        if (Object.keys(form).length > 0) {
            req.form = form;
        }
        if (typeof(body) === 'object' && !(body instanceof Buffer)) {
            req.json = true;
        }
        request(req, function(error, response, body) {
            if (error) {
                deferred.reject(error);
            } else {
                if (/^application\/(.*\\+)?json/.test(response.headers['content-type'])) {
                    try {
                        body = JSON.parse(body);
                    } catch (e) {}
                }
                if (response.statusCode === 204) {
                    deferred.resolve({
                        response: response
                    });
                } else if (response.statusCode >= 200 && response.statusCode <= 299) {
                    deferred.resolve({
                        response: response,
                        body: body
                    });
                } else {
                    deferred.reject({
                        response: response,
                        body: body
                    });
                }
            }
        });
    };

    /**
     * Set Token
     * @method
     * @name transmitsms#setToken
     * @param {string} value - token's value
     * @param {string} headerOrQueryName - the header or query name to send the token at
     * @param {boolean} isQuery - true if send the token as query param, otherwise, send as header param
     *
     */
    transmitsms.prototype.setToken = function(value, headerOrQueryName, isQuery) {
        this.token.value = value;
        this.token.headerOrQueryName = headerOrQueryName;
        this.token.isQuery = isQuery;
    };

    /**
     * Sends sms to the given number or list id
     * @method
     * @name transmitsms#send
     * @param {string} message - Message text
     * @param {string} to - Number or set of up to 10,000 numbers to send the SMS to. If your number set has some invalid numbers, they won’t cause validation error, but will be returned as ‘fails’ parameter of the response (see example 3).
     * @param {string} from - Set the alphanumeric Caller ID
     * @param {string} send_at - A time in the future to send the message
     * @param {string} list_id - This ID is the numerical reference to one of your recipient lists
     * @param {string} dlr_callback - A URL on your system which we can call to notify you of Delivery Receipts. If required, this Parameter can be different for each message sent and will take precedence over the DLR Callback URL supplied by you in the API Settings.
     * @param {string} reply_callback - A URL on your system which we can call to notify you of incoming messages. If required, this parameter can be different and will take precedence over the Reply Callback URL supplied by you on the API Settings.
     * @param {integer} validity - Specify the maximum time to attempt to deliver. In minutes, 0 (zero) implies no limit.
     * @param {string} replies_to_email - Specify an email address to send responses to this message. NOTE: specified email must be authorised to send messages via add-email or in your account under the 'Email SMS' section.
     * @param {string} from_shared - Forces sending via the shared number when you have virtual numbers
     * @param {string} countrycode - Formats the numbers given in to as internation format for this 2 letter country code.
     * @param {string} Authorization - in format key secret
     * 
     */
    transmitsms.prototype.send = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        var deferred = Q.defer();

        var domain = this.domain;
        var path = '/send-sms.json';

        var body;
        var queryParameters = {};
        var headers = {};
        var form = {};

        headers['Accept'] = ['application/json'];
        headers['Content-Type'] = ['application/x-www-form-urlencoded'];

        if (parameters['message'] !== undefined) {
            queryParameters['message'] = parameters['message'];
        }

        if (parameters['to'] !== undefined) {
            queryParameters['to'] = parameters['to'];
        }

        if (parameters['from'] !== undefined) {
            queryParameters['from'] = parameters['from'];
        }

        if (parameters['send_at'] !== undefined) {
            queryParameters['send_at'] = parameters['send_at'];
        }

        if (parameters['list_id'] !== undefined) {
            queryParameters['list_id'] = parameters['list_id'];
        }

        if (parameters['dlr_callback'] !== undefined) {
            queryParameters['dlr_callback'] = parameters['dlr_callback'];
        }

        if (parameters['reply_callback'] !== undefined) {
            queryParameters['reply_callback'] = parameters['reply_callback'];
        }

        if (parameters['validity'] !== undefined) {
            queryParameters['validity'] = parameters['validity'];
        }

        if (parameters['replies_to_email'] !== undefined) {
            queryParameters['replies_to_email'] = parameters['replies_to_email'];
        }

        if (parameters['from_shared'] !== undefined) {
            queryParameters['from_shared'] = parameters['from_shared'];
        }

        if (parameters['countrycode'] !== undefined) {
            queryParameters['countrycode'] = parameters['countrycode'];
        }

        if (parameters['Authorization'] !== undefined) {
            headers['Authorization'] = parameters['Authorization'];
        }

        if (parameters['Authorization'] === undefined) {
            deferred.reject(new Error('Missing required  parameter: Authorization'));
            return deferred.promise;
        }

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters)
                .forEach(function(parameterName) {
                    var parameter = parameters.$queryParameters[parameterName];
                    queryParameters[parameterName] = parameter;
                });
        }

        this.request('POST', domain + path, parameters, body, headers, queryParameters, form, deferred);

        return deferred.promise;
    };
    /**
     * Format and validate a given number.
     * @method
     * @name transmitsms#format_number
     * @param {string} msisdn - The number to check
     * @param {string} countrycode - 2 Letter countrycode to validate number against
     * @param {string} Authorization - in format key secret
     * 
     */
    transmitsms.prototype.format_number = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        var deferred = Q.defer();

        var domain = this.domain;
        var path = '/format-number.json';

        var body;
        var queryParameters = {};
        var headers = {};
        var form = {};

        headers['Accept'] = ['application/json'];
        headers['Content-Type'] = ['application/x-www-form-urlencoded'];

        if (parameters['msisdn'] !== undefined) {
            queryParameters['msisdn'] = parameters['msisdn'];
        }

        if (parameters['countrycode'] !== undefined) {
            queryParameters['countrycode'] = parameters['countrycode'];
        }

        if (parameters['Authorization'] !== undefined) {
            headers['Authorization'] = parameters['Authorization'];
        }

        if (parameters['Authorization'] === undefined) {
            deferred.reject(new Error('Missing required  parameter: Authorization'));
            return deferred.promise;
        }

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters)
                .forEach(function(parameterName) {
                    var parameter = parameters.$queryParameters[parameterName];
                    queryParameters[parameterName] = parameter;
                });
        }

        this.request('GET', domain + path, parameters, body, headers, queryParameters, form, deferred);

        return deferred.promise;
    };
    /**
     * Get information about a message you have sent.
     * @method
     * @name transmitsms#get_sms
     * @param {integer} message_id - Message ID
     * @param {string} Authorization - in format key secret
     * 
     */
    transmitsms.prototype.get_sms = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        var deferred = Q.defer();

        var domain = this.domain;
        var path = '/get-sms.json';

        var body;
        var queryParameters = {};
        var headers = {};
        var form = {};

        headers['Accept'] = ['application/json'];
        headers['Content-Type'] = ['application/x-www-form-urlencoded'];

        if (parameters['message_id'] !== undefined) {
            queryParameters['message_id'] = parameters['message_id'];
        }

        if (parameters['Authorization'] !== undefined) {
            headers['Authorization'] = parameters['Authorization'];
        }

        if (parameters['Authorization'] === undefined) {
            deferred.reject(new Error('Missing required  parameter: Authorization'));
            return deferred.promise;
        }

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters)
                .forEach(function(parameterName) {
                    var parameter = parameters.$queryParameters[parameterName];
                    queryParameters[parameterName] = parameter;
                });
        }

        this.request('GET', domain + path, parameters, body, headers, queryParameters, form, deferred);

        return deferred.promise;
    };
    /**
     * Pick up responses to messages you have sent. Filter by keyword or for just one phone number.
     * @method
     * @name transmitsms#get_sms_responses
     * @param {integer} message_id - Message ID
     * @param {integer} keyword_id - Keyword ID
     * @param {string} keyword - Keyword
     * @param {string} number - Filter results by response number
     * @param {string} msisdn - Filter results by a particular mobile number
     * @param {integer} page - Page number, for pagination
     * @param {integer} max - Maximum results returned per page
     * @param {boolean} include_original - include text of original message
     * @param {string} Authorization - in format key secret
     * 
     */
    transmitsms.prototype.get_sms_responses = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        var deferred = Q.defer();

        var domain = this.domain;
        var path = '/get-sms-responses.json';

        var body;
        var queryParameters = {};
        var headers = {};
        var form = {};

        headers['Accept'] = ['application/json'];
        headers['Content-Type'] = ['application/x-www-form-urlencoded'];

        if (parameters['message_id'] !== undefined) {
            queryParameters['message_id'] = parameters['message_id'];
        }

        if (parameters['keyword_id'] !== undefined) {
            queryParameters['keyword_id'] = parameters['keyword_id'];
        }

        if (parameters['keyword'] !== undefined) {
            queryParameters['keyword'] = parameters['keyword'];
        }

        if (parameters['number'] !== undefined) {
            queryParameters['number'] = parameters['number'];
        }

        if (parameters['msisdn'] !== undefined) {
            queryParameters['msisdn'] = parameters['msisdn'];
        }

        if (parameters['page'] !== undefined) {
            queryParameters['page'] = parameters['page'];
        }

        if (parameters['max'] !== undefined) {
            queryParameters['max'] = parameters['max'];
        }

        if (parameters['include_original'] !== undefined) {
            queryParameters['include_original'] = parameters['include_original'];
        }

        if (parameters['Authorization'] !== undefined) {
            headers['Authorization'] = parameters['Authorization'];
        }

        if (parameters['Authorization'] === undefined) {
            deferred.reject(new Error('Missing required  parameter: Authorization'));
            return deferred.promise;
        }

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters)
                .forEach(function(parameterName) {
                    var parameter = parameters.$queryParameters[parameterName];
                    queryParameters[parameterName] = parameter;
                });
        }

        this.request('GET', domain + path, parameters, body, headers, queryParameters, form, deferred);

        return deferred.promise;
    };
    /**
     * Pick up responses to messages you have sent. Instead of setting message ID, you should provide a time frame.
     * @method
     * @name transmitsms#get_user_sms_responses
     * @param {string} start - A timestamp to start the report from
     * @param {string} enddate - A timestamp to end the report at
     * @param {integer} page - Page number, for pagination
     * @param {integer} max - Maximum results returned per page
     * @param {string} keywords - Filter if keyword responses should be included. Can be: ‘only’ - only keyword responses will be included‘omit’ - only regular campaign responses will be included. ‘both’ - both keyword and campaign responses will be included (default)
     * @param {boolean} include_original - include text of original message
     * @param {string} Authorization - in format key secret
     * 
     */
    transmitsms.prototype.get_user_sms_responses = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        var deferred = Q.defer();

        var domain = this.domain;
        var path = '/get-user-sms-responses.json';

        var body;
        var queryParameters = {};
        var headers = {};
        var form = {};

        headers['Accept'] = ['application/json'];
        headers['Content-Type'] = ['application/x-www-form-urlencoded'];

        if (parameters['start'] !== undefined) {
            queryParameters['start'] = parameters['start'];
        }

        if (parameters['enddate'] !== undefined) {
            queryParameters['enddate'] = parameters['enddate'];
        }

        if (parameters['page'] !== undefined) {
            queryParameters['page'] = parameters['page'];
        }

        if (parameters['max'] !== undefined) {
            queryParameters['max'] = parameters['max'];
        }

        if (parameters['keywords'] !== undefined) {
            queryParameters['keywords'] = parameters['keywords'];
        }

        if (parameters['include_original'] !== undefined) {
            queryParameters['include_original'] = parameters['include_original'];
        }

        if (parameters['Authorization'] !== undefined) {
            headers['Authorization'] = parameters['Authorization'];
        }

        if (parameters['Authorization'] === undefined) {
            deferred.reject(new Error('Missing required  parameter: Authorization'));
            return deferred.promise;
        }

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters)
                .forEach(function(parameterName) {
                    var parameter = parameters.$queryParameters[parameterName];
                    queryParameters[parameterName] = parameter;
                });
        }

        this.request('GET', domain + path, parameters, body, headers, queryParameters, form, deferred);

        return deferred.promise;
    };
    /**
     * Get a list of recipients from a message send. Get up to date information such as opt-out status and delivery status.
     * @method
     * @name transmitsms#get_sms_sent
     * @param {integer} message_id - Message ID's are made up of digits
     * @param {string} optouts - Whether to include optouts. Valid options are: only - only get optouts, omit - do not get optouts, include - get all recipients including optouts (default)
     * @param {integer} page - Page number, for pagination
     * @param {integer} max - Maximum results returned per page
     * @param {string} delivery - Only show messages with requested delivery status. Valid options are: delivered - only show delivered messages, failed - only show failed messages, pending - only show pending messages
     * @param {string} Authorization - in format key secret
     * 
     */
    transmitsms.prototype.get_sms_sent = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        var deferred = Q.defer();

        var domain = this.domain;
        var path = '/get-sms-sent.json';

        var body;
        var queryParameters = {};
        var headers = {};
        var form = {};

        headers['Accept'] = ['application/json'];
        headers['Content-Type'] = ['application/x-www-form-urlencoded'];

        if (parameters['message_id'] !== undefined) {
            queryParameters['message_id'] = parameters['message_id'];
        }

        if (parameters['optouts'] !== undefined) {
            queryParameters['optouts'] = parameters['optouts'];
        }

        if (parameters['page'] !== undefined) {
            queryParameters['page'] = parameters['page'];
        }

        if (parameters['max'] !== undefined) {
            queryParameters['max'] = parameters['max'];
        }

        if (parameters['delivery'] !== undefined) {
            queryParameters['delivery'] = parameters['delivery'];
        }

        if (parameters['Authorization'] !== undefined) {
            headers['Authorization'] = parameters['Authorization'];
        }

        if (parameters['Authorization'] === undefined) {
            deferred.reject(new Error('Missing required  parameter: Authorization'));
            return deferred.promise;
        }

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters)
                .forEach(function(parameterName) {
                    var parameter = parameters.$queryParameters[parameterName];
                    queryParameters[parameterName] = parameter;
                });
        }

        this.request('GET', domain + path, parameters, body, headers, queryParameters, form, deferred);

        return deferred.promise;
    };
    /**
     * Cancel a message you have scheduled to be sent in the future.
     * @method
     * @name transmitsms#cancel_sms
     * @param {integer} id - Message ID
     * @param {string} Authorization - in format key secret
     * 
     */
    transmitsms.prototype.cancel_sms = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        var deferred = Q.defer();

        var domain = this.domain;
        var path = '/cancel-sms.json';

        var body;
        var queryParameters = {};
        var headers = {};
        var form = {};

        headers['Accept'] = ['application/json'];
        headers['Content-Type'] = ['application/x-www-form-urlencoded'];

        if (parameters['id'] !== undefined) {
            queryParameters['id'] = parameters['id'];
        }

        if (parameters['Authorization'] !== undefined) {
            headers['Authorization'] = parameters['Authorization'];
        }

        if (parameters['Authorization'] === undefined) {
            deferred.reject(new Error('Missing required  parameter: Authorization'));
            return deferred.promise;
        }

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters)
                .forEach(function(parameterName) {
                    var parameter = parameters.$queryParameters[parameterName];
                    queryParameters[parameterName] = parameter;
                });
        }

        this.request('GET', domain + path, parameters, body, headers, queryParameters, form, deferred);

        return deferred.promise;
    };
    /**
     * message-reply
     * @method
     * @name transmitsms#message_reply
     * @param {string} message - Message text
     * @param {string} from - Set the alphanumeric Caller ID
     * @param {string} to - Number or set of up to 10,000 numbers to send the SMS to. If your number set has some invalid numbers, they won’t cause validation error, but will be returned as ‘fails’ parameter of the response (see example 3).
     * @param {string} Authorization - in format key secret
     * 
     */
    transmitsms.prototype.message_reply = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        var deferred = Q.defer();

        var domain = this.domain;
        var path = '/message-reply.json';

        var body;
        var queryParameters = {};
        var headers = {};
        var form = {};

        headers['Accept'] = ['application/json'];
        headers['Content-Type'] = ['application/x-www-form-urlencoded'];

        if (parameters['message'] !== undefined) {
            queryParameters['message'] = parameters['message'];
        }

        if (parameters['from'] !== undefined) {
            queryParameters['from'] = parameters['from'];
        }

        if (parameters['to'] !== undefined) {
            queryParameters['to'] = parameters['to'];
        }

        if (parameters['Authorization'] !== undefined) {
            headers['Authorization'] = parameters['Authorization'];
        }

        if (parameters['Authorization'] === undefined) {
            deferred.reject(new Error('Missing required  parameter: Authorization'));
            return deferred.promise;
        }

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters)
                .forEach(function(parameterName) {
                    var parameter = parameters.$queryParameters[parameterName];
                    queryParameters[parameterName] = parameter;
                });
        }

        this.request('POST', domain + path, parameters, body, headers, queryParameters, form, deferred);

        return deferred.promise;
    };
    /**
     * Format and validate a given number.
     * @method
     * @name transmitsms#get_message_log
     * @param {int} message_id - The message to retrieve
     * @param {string} mobile - The mobile used in message
     * @param {string} Authorization - in format key secret
     * 
     */
    transmitsms.prototype.get_message_log = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        var deferred = Q.defer();

        var domain = this.domain;
        var path = '/get-message-log.json';

        var body;
        var queryParameters = {};
        var headers = {};
        var form = {};

        headers['Accept'] = ['application/json'];
        headers['Content-Type'] = ['application/x-www-form-urlencoded'];

        if (parameters['message_id'] !== undefined) {
            queryParameters['message_id'] = parameters['message_id'];
        }

        if (parameters['mobile'] !== undefined) {
            queryParameters['mobile'] = parameters['mobile'];
        }

        if (parameters['Authorization'] !== undefined) {
            headers['Authorization'] = parameters['Authorization'];
        }

        if (parameters['Authorization'] === undefined) {
            deferred.reject(new Error('Missing required  parameter: Authorization'));
            return deferred.promise;
        }

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters)
                .forEach(function(parameterName) {
                    var parameter = parameters.$queryParameters[parameterName];
                    queryParameters[parameterName] = parameter;
                });
        }

        this.request('GET', domain + path, parameters, body, headers, queryParameters, form, deferred);

        return deferred.promise;
    };
    /**
     * Get detailed information about a client.
     * @method
     * @name transmitsms#get_client
     * @param {string} client_id - The ID of the client
     * @param {string} Authorization - in format key secret
     * 
     */
    transmitsms.prototype.get_client = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        var deferred = Q.defer();

        var domain = this.domain;
        var path = '/get-client.json';

        var body;
        var queryParameters = {};
        var headers = {};
        var form = {};

        headers['Accept'] = ['application/json'];
        headers['Content-Type'] = ['application/x-www-form-urlencoded'];

        if (parameters['client_id'] !== undefined) {
            queryParameters['client_id'] = parameters['client_id'];
        }

        if (parameters['client_id'] === undefined) {
            deferred.reject(new Error('Missing required  parameter: client_id'));
            return deferred.promise;
        }

        if (parameters['Authorization'] !== undefined) {
            headers['Authorization'] = parameters['Authorization'];
        }

        if (parameters['Authorization'] === undefined) {
            deferred.reject(new Error('Missing required  parameter: Authorization'));
            return deferred.promise;
        }

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters)
                .forEach(function(parameterName) {
                    var parameter = parameters.$queryParameters[parameterName];
                    queryParameters[parameterName] = parameter;
                });
        }

        this.request('GET', domain + path, parameters, body, headers, queryParameters, form, deferred);

        return deferred.promise;
    };
    /**
     * Get a list of all clients.
     * @method
     * @name transmitsms#get_clients
     * @param {string} page - Page number, for pagination
     * @param {string} max - Maximum results returned per page
     * @param {string} Authorization - in format key secret
     * 
     */
    transmitsms.prototype.get_clients = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        var deferred = Q.defer();

        var domain = this.domain;
        var path = '/get-clients.json';

        var body;
        var queryParameters = {};
        var headers = {};
        var form = {};

        headers['Accept'] = ['application/json'];
        headers['Content-Type'] = ['application/x-www-form-urlencoded'];

        if (parameters['page'] !== undefined) {
            queryParameters['page'] = parameters['page'];
        }

        if (parameters['page'] === undefined) {
            deferred.reject(new Error('Missing required  parameter: page'));
            return deferred.promise;
        }

        if (parameters['max'] !== undefined) {
            queryParameters['max'] = parameters['max'];
        }

        if (parameters['max'] === undefined) {
            deferred.reject(new Error('Missing required  parameter: max'));
            return deferred.promise;
        }

        if (parameters['Authorization'] !== undefined) {
            headers['Authorization'] = parameters['Authorization'];
        }

        if (parameters['Authorization'] === undefined) {
            deferred.reject(new Error('Missing required  parameter: Authorization'));
            return deferred.promise;
        }

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters)
                .forEach(function(parameterName) {
                    var parameter = parameters.$queryParameters[parameterName];
                    queryParameters[parameterName] = parameter;
                });
        }

        this.request('GET', domain + path, parameters, body, headers, queryParameters, form, deferred);

        return deferred.promise;
    };
    /**
     * Add a new client.
     * @method
     * @name transmitsms#add_client
     * @param {string} name - Client company name
     * @param {string} contact - Contact name
     * @param {string} email - Client email address
     * @param {string} password - Client password
     * @param {string} msisdn - Client phone number
     * @param {string} timezone - A valid timezone, Australia/Sydney. Defaults to your own
     * @param {string} client_pays - Set to true if the client will pay (the default) or false if you will pay
     * @param {string} sms_margin - The number of cents to add to the base SMS price. A decimal value
     * @param {string} number_margin - The number of cents to add to the base Number price. A decimal value
     * @param {string} Authorization - in format key secret
     * 
     */
    transmitsms.prototype.add_client = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        var deferred = Q.defer();

        var domain = this.domain;
        var path = '/add-client.json';

        var body;
        var queryParameters = {};
        var headers = {};
        var form = {};

        headers['Accept'] = ['application/json'];
        headers['Content-Type'] = ['application/x-www-form-urlencoded'];

        if (parameters['name'] !== undefined) {
            queryParameters['name'] = parameters['name'];
        }

        if (parameters['name'] === undefined) {
            deferred.reject(new Error('Missing required  parameter: name'));
            return deferred.promise;
        }

        if (parameters['contact'] !== undefined) {
            queryParameters['contact'] = parameters['contact'];
        }

        if (parameters['email'] !== undefined) {
            queryParameters['email'] = parameters['email'];
        }

        if (parameters['email'] === undefined) {
            deferred.reject(new Error('Missing required  parameter: email'));
            return deferred.promise;
        }

        if (parameters['password'] !== undefined) {
            queryParameters['password'] = parameters['password'];
        }

        if (parameters['password'] === undefined) {
            deferred.reject(new Error('Missing required  parameter: password'));
            return deferred.promise;
        }

        if (parameters['msisdn'] !== undefined) {
            queryParameters['msisdn'] = parameters['msisdn'];
        }

        if (parameters['msisdn'] === undefined) {
            deferred.reject(new Error('Missing required  parameter: msisdn'));
            return deferred.promise;
        }

        if (parameters['timezone'] !== undefined) {
            queryParameters['timezone'] = parameters['timezone'];
        }

        if (parameters['client_pays'] !== undefined) {
            queryParameters['client_pays'] = parameters['client_pays'];
        }

        if (parameters['sms_margin'] !== undefined) {
            queryParameters['sms_margin'] = parameters['sms_margin'];
        }

        if (parameters['number_margin'] !== undefined) {
            queryParameters['number_margin'] = parameters['number_margin'];
        }

        if (parameters['Authorization'] !== undefined) {
            headers['Authorization'] = parameters['Authorization'];
        }

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters)
                .forEach(function(parameterName) {
                    var parameter = parameters.$queryParameters[parameterName];
                    queryParameters[parameterName] = parameter;
                });
        }

        this.request('POST', domain + path, parameters, body, headers, queryParameters, form, deferred);

        return deferred.promise;
    };
    /**
     * Edit an existing client
     * @method
     * @name transmitsms#edit_client
     * @param {string} client_id - The ID of the client
     * @param {string} name - Client company name
     * @param {string} contact - Contact name
     * @param {string} email - Client email address
     * @param {string} password - Client password
     * @param {string} msisdn - Client phone number
     * @param {string} timezone - A valid timezone, Australia/Sydney. Defaults to your own
     * @param {string} client_pays - Set to true if the client will pay (the default) or false if you will pay
     * @param {string} sms_margin - The number of cents to add to the base SMS price. A decimal value
     * @param {string} Authorization - in format key secret
     * 
     */
    transmitsms.prototype.edit_client = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        var deferred = Q.defer();

        var domain = this.domain;
        var path = '/edit-client.json';

        var body;
        var queryParameters = {};
        var headers = {};
        var form = {};

        headers['Accept'] = ['application/json'];
        headers['Content-Type'] = ['application/x-www-form-urlencoded'];

        if (parameters['client_id'] !== undefined) {
            queryParameters['client_id'] = parameters['client_id'];
        }

        if (parameters['client_id'] === undefined) {
            deferred.reject(new Error('Missing required  parameter: client_id'));
            return deferred.promise;
        }

        if (parameters['name'] !== undefined) {
            queryParameters['name'] = parameters['name'];
        }

        if (parameters['contact'] !== undefined) {
            queryParameters['contact'] = parameters['contact'];
        }

        if (parameters['email'] !== undefined) {
            queryParameters['email'] = parameters['email'];
        }

        if (parameters['password'] !== undefined) {
            queryParameters['password'] = parameters['password'];
        }

        if (parameters['msisdn'] !== undefined) {
            queryParameters['msisdn'] = parameters['msisdn'];
        }

        if (parameters['timezone'] !== undefined) {
            queryParameters['timezone'] = parameters['timezone'];
        }

        if (parameters['client_pays'] !== undefined) {
            queryParameters['client_pays'] = parameters['client_pays'];
        }

        if (parameters['sms_margin'] !== undefined) {
            queryParameters['sms_margin'] = parameters['sms_margin'];
        }

        if (parameters['Authorization'] !== undefined) {
            headers['Authorization'] = parameters['Authorization'];
        }

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters)
                .forEach(function(parameterName) {
                    var parameter = parameters.$queryParameters[parameterName];
                    queryParameters[parameterName] = parameter;
                });
        }

        this.request('PUT', domain + path, parameters, body, headers, queryParameters, form, deferred);

        return deferred.promise;
    };
    /**
     * Get a list of transactions for an account.
     * @method
     * @name transmitsms#get_transactions
     * @param {string} client_id - Only retrieve records for a particular client
     * @param {string} start - A timestamp to start the report from
     * @param {string} end - A timestamp to end the report at
     * @param {string} page - Page number, for pagination
     * @param {string} max - Maximum results returned per page
     * @param {string} Authorization - in format key secret
     * 
     */
    transmitsms.prototype.get_transactions = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        var deferred = Q.defer();

        var domain = this.domain;
        var path = '/get-transactions.json';

        var body;
        var queryParameters = {};
        var headers = {};
        var form = {};

        headers['Accept'] = ['application/json'];
        headers['Content-Type'] = ['application/x-www-form-urlencoded'];

        if (parameters['client_id'] !== undefined) {
            queryParameters['client_id'] = parameters['client_id'];
        }

        if (parameters['client_id'] === undefined) {
            deferred.reject(new Error('Missing required  parameter: client_id'));
            return deferred.promise;
        }

        if (parameters['start'] !== undefined) {
            queryParameters['start'] = parameters['start'];
        }

        if (parameters['start'] === undefined) {
            deferred.reject(new Error('Missing required  parameter: start'));
            return deferred.promise;
        }

        if (parameters['end'] !== undefined) {
            queryParameters['end'] = parameters['end'];
        }

        if (parameters['end'] === undefined) {
            deferred.reject(new Error('Missing required  parameter: end'));
            return deferred.promise;
        }

        if (parameters['page'] !== undefined) {
            queryParameters['page'] = parameters['page'];
        }

        if (parameters['page'] === undefined) {
            deferred.reject(new Error('Missing required  parameter: page'));
            return deferred.promise;
        }

        if (parameters['max'] !== undefined) {
            queryParameters['max'] = parameters['max'];
        }

        if (parameters['max'] === undefined) {
            deferred.reject(new Error('Missing required  parameter: max'));
            return deferred.promise;
        }

        if (parameters['Authorization'] !== undefined) {
            headers['Authorization'] = parameters['Authorization'];
        }

        if (parameters['Authorization'] === undefined) {
            deferred.reject(new Error('Missing required  parameter: Authorization'));
            return deferred.promise;
        }

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters)
                .forEach(function(parameterName) {
                    var parameter = parameters.$queryParameters[parameterName];
                    queryParameters[parameterName] = parameter;
                });
        }

        this.request('GET', domain + path, parameters, body, headers, queryParameters, form, deferred);

        return deferred.promise;
    };
    /**
     * Get a list of transactions for an account.
     * @method
     * @name transmitsms#get_transaction
     * @param {string} id - Transaction ID
     * @param {string} Authorization - in format key secret
     * 
     */
    transmitsms.prototype.get_transaction = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        var deferred = Q.defer();

        var domain = this.domain;
        var path = '/get-transaction.json';

        var body;
        var queryParameters = {};
        var headers = {};
        var form = {};

        headers['Accept'] = ['application/json'];
        headers['Content-Type'] = ['application/x-www-form-urlencoded'];

        if (parameters['id'] !== undefined) {
            queryParameters['id'] = parameters['id'];
        }

        if (parameters['id'] === undefined) {
            deferred.reject(new Error('Missing required  parameter: id'));
            return deferred.promise;
        }

        if (parameters['Authorization'] !== undefined) {
            headers['Authorization'] = parameters['Authorization'];
        }

        if (parameters['Authorization'] === undefined) {
            deferred.reject(new Error('Missing required  parameter: Authorization'));
            return deferred.promise;
        }

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters)
                .forEach(function(parameterName) {
                    var parameter = parameters.$queryParameters[parameterName];
                    queryParameters[parameterName] = parameter;
                });
        }

        this.request('GET', domain + path, parameters, body, headers, queryParameters, form, deferred);

        return deferred.promise;
    };
    /**
     * Get a summary of your account balance.
     * @method
     * @name transmitsms#get_balance
     * @param {string} Authorization - API credentials
     * 
     */
    transmitsms.prototype.get_balance = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        var deferred = Q.defer();

        var domain = this.domain;
        var path = '/get-balance.json';

        var body;
        var queryParameters = {};
        var headers = {};
        var form = {};

        headers['Accept'] = ['application/json'];
        headers['Content-Type'] = ['application/x-www-form-urlencoded'];

        if (parameters['Authorization'] !== undefined) {
            headers['Authorization'] = parameters['Authorization'];
        }

        if (parameters['Authorization'] === undefined) {
            deferred.reject(new Error('Missing required  parameter: Authorization'));
            return deferred.promise;
        }

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters)
                .forEach(function(parameterName) {
                    var parameter = parameters.$queryParameters[parameterName];
                    queryParameters[parameterName] = parameter;
                });
        }

        this.request('POST', domain + path, parameters, body, headers, queryParameters, form, deferred);

        return deferred.promise;
    };
    /**
     * Authorise an email address for sending Email to SMS
     * @method
     * @name transmitsms#add_email
     * @param {string} email - Email address to register. You may also register a wild-card email which allows any user on the same domain to use Email to SMS.
     * @param {integer} max_sms - The maximum number of SMS messages to send from one email message sent from this email address.
     * @param {integer} number - Optional dedicated virtual number virtual number
     * @param {string} Authorization - API credentials
     * 
     */
    transmitsms.prototype.add_email = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        var deferred = Q.defer();

        var domain = this.domain;
        var path = '/add-email.json';

        var body;
        var queryParameters = {};
        var headers = {};
        var form = {};

        headers['Accept'] = ['application/json'];
        headers['Content-Type'] = ['application/x-www-form-urlencoded'];

        if (parameters['email'] !== undefined) {
            queryParameters['email'] = parameters['email'];
        }

        if (parameters['email'] === undefined) {
            deferred.reject(new Error('Missing required  parameter: email'));
            return deferred.promise;
        }

        if (parameters['max_sms'] !== undefined) {
            queryParameters['max_sms'] = parameters['max_sms'];
        }

        if (parameters['number'] !== undefined) {
            queryParameters['number'] = parameters['number'];
        }

        if (parameters['Authorization'] !== undefined) {
            headers['Authorization'] = parameters['Authorization'];
        }

        if (parameters['Authorization'] === undefined) {
            deferred.reject(new Error('Missing required  parameter: Authorization'));
            return deferred.promise;
        }

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters)
                .forEach(function(parameterName) {
                    var parameter = parameters.$queryParameters[parameterName];
                    queryParameters[parameterName] = parameter;
                });
        }

        this.request('POST', domain + path, parameters, body, headers, queryParameters, form, deferred);

        return deferred.promise;
    };
    /**
     * Remove an email address from the Email to SMS authorised list.
     * @method
     * @name transmitsms#delete_email
     * @param {string} email - Email address to remove. You may also use a wild-card email which removes all emails on that domain.
     * @param {string} Authorization - in format key secret
     * 
     */
    transmitsms.prototype.delete_email = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        var deferred = Q.defer();

        var domain = this.domain;
        var path = '/delete-email.json';

        var body;
        var queryParameters = {};
        var headers = {};
        var form = {};

        headers['Accept'] = ['application/json'];
        headers['Content-Type'] = ['application/x-www-form-urlencoded'];

        if (parameters['email'] !== undefined) {
            queryParameters['email'] = parameters['email'];
        }

        if (parameters['email'] === undefined) {
            deferred.reject(new Error('Missing required  parameter: email'));
            return deferred.promise;
        }

        if (parameters['Authorization'] !== undefined) {
            headers['Authorization'] = parameters['Authorization'];
        }

        if (parameters['Authorization'] === undefined) {
            deferred.reject(new Error('Missing required  parameter: Authorization'));
            return deferred.promise;
        }

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters)
                .forEach(function(parameterName) {
                    var parameter = parameters.$queryParameters[parameterName];
                    queryParameters[parameterName] = parameter;
                });
        }

        this.request('POST', domain + path, parameters, body, headers, queryParameters, form, deferred);

        return deferred.promise;
    };
    /**
     * Add a keyword to an existing virtual number
     * @method
     * @name transmitsms#add_keyword
     * @param {string} keyword - The first word of a text message
     * @param {integer} number - The dedicated virtual number that the keyword belongs to
     * @param {string} reference - Your own reference (up to 100 characters)
     * @param {integer} list_id - ID of a list to add respondents to, list ID's can be found in the title of a list or in the list page URL
     * @param {string} welcome_message - SMS message to send to new members
     * @param {string} members_message - SMS message to existing members
     * @param {boolean} activate - Whether to make the keyword active immediately.
     * @param {string} forward_url - Forward messages to a URL
     * @param {string} forward_email - Forward messages to a set of email addresses
     * @param {string} forward_sms - Forward messages to a set of msisdns
     * @param {string} Authorization - API credentials
     * 
     */
    transmitsms.prototype.add_keyword = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        var deferred = Q.defer();

        var domain = this.domain;
        var path = '/add-keyword.json';

        var body;
        var queryParameters = {};
        var headers = {};
        var form = {};

        headers['Accept'] = ['application/json'];
        headers['Content-Type'] = ['application/x-www-form-urlencoded'];

        if (parameters['keyword'] !== undefined) {
            queryParameters['keyword'] = parameters['keyword'];
        }

        if (parameters['keyword'] === undefined) {
            deferred.reject(new Error('Missing required  parameter: keyword'));
            return deferred.promise;
        }

        if (parameters['number'] !== undefined) {
            queryParameters['number'] = parameters['number'];
        }

        if (parameters['number'] === undefined) {
            deferred.reject(new Error('Missing required  parameter: number'));
            return deferred.promise;
        }

        if (parameters['reference'] !== undefined) {
            queryParameters['reference'] = parameters['reference'];
        }

        if (parameters['list_id'] !== undefined) {
            queryParameters['list_id'] = parameters['list_id'];
        }

        if (parameters['welcome_message'] !== undefined) {
            queryParameters['welcome_message'] = parameters['welcome_message'];
        }

        if (parameters['members_message'] !== undefined) {
            queryParameters['members_message'] = parameters['members_message'];
        }

        if (parameters['activate'] !== undefined) {
            queryParameters['activate'] = parameters['activate'];
        }

        if (parameters['forward_url'] !== undefined) {
            queryParameters['forward_url'] = parameters['forward_url'];
        }

        if (parameters['forward_email'] !== undefined) {
            queryParameters['forward_email'] = parameters['forward_email'];
        }

        if (parameters['forward_sms'] !== undefined) {
            queryParameters['forward_sms'] = parameters['forward_sms'];
        }

        if (parameters['Authorization'] !== undefined) {
            headers['Authorization'] = parameters['Authorization'];
        }

        if (parameters['Authorization'] === undefined) {
            deferred.reject(new Error('Missing required  parameter: Authorization'));
            return deferred.promise;
        }

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters)
                .forEach(function(parameterName) {
                    var parameter = parameters.$queryParameters[parameterName];
                    queryParameters[parameterName] = parameter;
                });
        }

        this.request('POST', domain + path, parameters, body, headers, queryParameters, form, deferred);

        return deferred.promise;
    };
    /**
     * Edit an existing keyword.
     * @method
     * @name transmitsms#edit_keyword
     * @param {string} keyword - The first word of a text message
     * @param {integer} number - The dedicated virtual number that the keyword belongs to
     * @param {string} reference - Your own reference (up to 100 characters)
     * @param {integer} list_id - ID of a list to add respondents to, list ID's can be found in the title of a list or in the list page URL
     * @param {string} welcome_message - SMS message to send to new members
     * @param {string} members_message - SMS message to existing members
     * @param {boolean} activate - Whether to make the keyword active immediately.
     * @param {string} forward_url - Forward messages to a URL
     * @param {string} forward_email - Forward messages to a set of email addresses
     * @param {string} forward_sms - Forward messages to a set of msisdns
     * @param {string} Authorization - API credentials
     * 
     */
    transmitsms.prototype.edit_keyword = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        var deferred = Q.defer();

        var domain = this.domain;
        var path = '/edit-keyword.json';

        var body;
        var queryParameters = {};
        var headers = {};
        var form = {};

        headers['Accept'] = ['application/json'];
        headers['Content-Type'] = ['application/x-www-form-urlencoded'];

        if (parameters['keyword'] !== undefined) {
            queryParameters['keyword'] = parameters['keyword'];
        }

        if (parameters['keyword'] === undefined) {
            deferred.reject(new Error('Missing required  parameter: keyword'));
            return deferred.promise;
        }

        if (parameters['number'] !== undefined) {
            queryParameters['number'] = parameters['number'];
        }

        if (parameters['number'] === undefined) {
            deferred.reject(new Error('Missing required  parameter: number'));
            return deferred.promise;
        }

        if (parameters['reference'] !== undefined) {
            queryParameters['reference'] = parameters['reference'];
        }

        if (parameters['list_id'] !== undefined) {
            queryParameters['list_id'] = parameters['list_id'];
        }

        if (parameters['welcome_message'] !== undefined) {
            queryParameters['welcome_message'] = parameters['welcome_message'];
        }

        if (parameters['members_message'] !== undefined) {
            queryParameters['members_message'] = parameters['members_message'];
        }

        if (parameters['activate'] !== undefined) {
            queryParameters['activate'] = parameters['activate'];
        }

        if (parameters['forward_url'] !== undefined) {
            queryParameters['forward_url'] = parameters['forward_url'];
        }

        if (parameters['forward_email'] !== undefined) {
            queryParameters['forward_email'] = parameters['forward_email'];
        }

        if (parameters['forward_sms'] !== undefined) {
            queryParameters['forward_sms'] = parameters['forward_sms'];
        }

        if (parameters['Authorization'] !== undefined) {
            headers['Authorization'] = parameters['Authorization'];
        }

        if (parameters['Authorization'] === undefined) {
            deferred.reject(new Error('Missing required  parameter: Authorization'));
            return deferred.promise;
        }

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters)
                .forEach(function(parameterName) {
                    var parameter = parameters.$queryParameters[parameterName];
                    queryParameters[parameterName] = parameter;
                });
        }

        this.request('POST', domain + path, parameters, body, headers, queryParameters, form, deferred);

        return deferred.promise;
    };
    /**
     * Get a list of existing keywords.
     * @method
     * @name transmitsms#get_keywords
     * @param {integer} number - Filter the list by virtual number
     * @param {integer} page - Page number, for pagination
     * @param {integer} max -   Maximum results returned per page
     * @param {string} Authorization - in format key secret
     * 
     */
    transmitsms.prototype.get_keywords = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        var deferred = Q.defer();

        var domain = this.domain;
        var path = '/get-keywords.json';

        var body;
        var queryParameters = {};
        var headers = {};
        var form = {};

        headers['Accept'] = ['application/json'];
        headers['Content-Type'] = ['application/x-www-form-urlencoded'];

        if (parameters['number'] !== undefined) {
            queryParameters['number'] = parameters['number'];
        }

        if (parameters['page'] !== undefined) {
            queryParameters['page'] = parameters['page'];
        }

        if (parameters['max'] !== undefined) {
            queryParameters['max'] = parameters['max'];
        }

        if (parameters['Authorization'] !== undefined) {
            headers['Authorization'] = parameters['Authorization'];
        }

        if (parameters['Authorization'] === undefined) {
            deferred.reject(new Error('Missing required  parameter: Authorization'));
            return deferred.promise;
        }

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters)
                .forEach(function(parameterName) {
                    var parameter = parameters.$queryParameters[parameterName];
                    queryParameters[parameterName] = parameter;
                });
        }

        this.request('POST', domain + path, parameters, body, headers, queryParameters, form, deferred);

        return deferred.promise;
    };
    /**
     * Create a new list including the ability to add custom fields.
     * @method
     * @name transmitsms#add_list
     * @param {string} name - A unique name for the list
     * @param {string} field_1 - Custom field value 1
     * @param {string} field_2 - Custom field value 2
     * @param {string} field_3 - Custom field value 3
     * @param {string} field_4 - Custom field value 4
     * @param {string} field_5 - Custom field value 5
     * @param {string} field_6 - Custom field value 6
     * @param {string} field_7 - Custom field value 7
     * @param {string} field_8 - Custom field value 8
     * @param {string} field_9 - Custom field value 9
     * @param {string} field_10 - Custom field value 10
     * @param {string} Authorization - in format key secret
     * 
     */
    transmitsms.prototype.add_list = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        var deferred = Q.defer();

        var domain = this.domain;
        var path = '/add-list.json';

        var body;
        var queryParameters = {};
        var headers = {};
        var form = {};

        headers['Accept'] = ['application/json'];
        headers['Content-Type'] = ['application/x-www-form-urlencoded'];

        if (parameters['name'] !== undefined) {
            queryParameters['name'] = parameters['name'];
        }

        if (parameters['name'] === undefined) {
            deferred.reject(new Error('Missing required  parameter: name'));
            return deferred.promise;
        }

        if (parameters['field_1'] !== undefined) {
            queryParameters['field_1'] = parameters['field_1'];
        }

        if (parameters['field_2'] !== undefined) {
            queryParameters['field_2'] = parameters['field_2'];
        }

        if (parameters['field_3'] !== undefined) {
            queryParameters['field_3'] = parameters['field_3'];
        }

        if (parameters['field_4'] !== undefined) {
            queryParameters['field_4'] = parameters['field_4'];
        }

        if (parameters['field_5'] !== undefined) {
            queryParameters['field_5'] = parameters['field_5'];
        }

        if (parameters['field_6'] !== undefined) {
            queryParameters['field_6'] = parameters['field_6'];
        }

        if (parameters['field_7'] !== undefined) {
            queryParameters['field_7'] = parameters['field_7'];
        }

        if (parameters['field_8'] !== undefined) {
            queryParameters['field_8'] = parameters['field_8'];
        }

        if (parameters['field_9'] !== undefined) {
            queryParameters['field_9'] = parameters['field_9'];
        }

        if (parameters['field_10'] !== undefined) {
            queryParameters['field_10'] = parameters['field_10'];
        }

        if (parameters['Authorization'] !== undefined) {
            headers['Authorization'] = parameters['Authorization'];
        }

        if (parameters['Authorization'] === undefined) {
            deferred.reject(new Error('Missing required  parameter: Authorization'));
            return deferred.promise;
        }

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters)
                .forEach(function(parameterName) {
                    var parameter = parameters.$queryParameters[parameterName];
                    queryParameters[parameterName] = parameter;
                });
        }

        this.request('POST', domain + path, parameters, body, headers, queryParameters, form, deferred);

        return deferred.promise;
    };
    /**
     * Add a member to a list.
     * @method
     * @name transmitsms#add_to_list
     * @param {integer} list_id - ID of the list to add to
     * @param {integer} msisdn - Mobile number of the member
     * @param {string} first_name - First name of the member
     * @param {string} last_name - Last name of the member
     * @param {string} field_1 - Custom field value 1
     * @param {string} field_2 - Custom field value 2
     * @param {string} field_3 - Custom field value 3
     * @param {string} field_4 - Custom field value 4
     * @param {string} field_5 - Custom field value 5
     * @param {string} field_6 - Custom field value 6
     * @param {string} field_7 - Custom field value 7
     * @param {string} field_8 - Custom field value 8
     * @param {string} field_9 - Custom field value 9
     * @param {string} field_10 - Custom field value 10
     * @param {string} Authorization - in format key secret
     * 
     */
    transmitsms.prototype.add_to_list = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        var deferred = Q.defer();

        var domain = this.domain;
        var path = '/add-to-list.json';

        var body;
        var queryParameters = {};
        var headers = {};
        var form = {};

        headers['Accept'] = ['application/json'];
        headers['Content-Type'] = ['application/x-www-form-urlencoded'];

        if (parameters['list_id'] !== undefined) {
            queryParameters['list_id'] = parameters['list_id'];
        }

        if (parameters['list_id'] === undefined) {
            deferred.reject(new Error('Missing required  parameter: list_id'));
            return deferred.promise;
        }

        if (parameters['msisdn'] !== undefined) {
            queryParameters['msisdn'] = parameters['msisdn'];
        }

        if (parameters['msisdn'] === undefined) {
            deferred.reject(new Error('Missing required  parameter: msisdn'));
            return deferred.promise;
        }

        if (parameters['first_name'] !== undefined) {
            queryParameters['first_name'] = parameters['first_name'];
        }

        if (parameters['last_name'] !== undefined) {
            queryParameters['last_name'] = parameters['last_name'];
        }

        if (parameters['field_1'] !== undefined) {
            queryParameters['field_1'] = parameters['field_1'];
        }

        if (parameters['field_2'] !== undefined) {
            queryParameters['field_2'] = parameters['field_2'];
        }

        if (parameters['field_3'] !== undefined) {
            queryParameters['field_3'] = parameters['field_3'];
        }

        if (parameters['field_4'] !== undefined) {
            queryParameters['field_4'] = parameters['field_4'];
        }

        if (parameters['field_5'] !== undefined) {
            queryParameters['field_5'] = parameters['field_5'];
        }

        if (parameters['field_6'] !== undefined) {
            queryParameters['field_6'] = parameters['field_6'];
        }

        if (parameters['field_7'] !== undefined) {
            queryParameters['field_7'] = parameters['field_7'];
        }

        if (parameters['field_8'] !== undefined) {
            queryParameters['field_8'] = parameters['field_8'];
        }

        if (parameters['field_9'] !== undefined) {
            queryParameters['field_9'] = parameters['field_9'];
        }

        if (parameters['field_10'] !== undefined) {
            queryParameters['field_10'] = parameters['field_10'];
        }

        if (parameters['Authorization'] !== undefined) {
            headers['Authorization'] = parameters['Authorization'];
        }

        if (parameters['Authorization'] === undefined) {
            deferred.reject(new Error('Missing required  parameter: Authorization'));
            return deferred.promise;
        }

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters)
                .forEach(function(parameterName) {
                    var parameter = parameters.$queryParameters[parameterName];
                    queryParameters[parameterName] = parameter;
                });
        }

        this.request('POST', domain + path, parameters, body, headers, queryParameters, form, deferred);

        return deferred.promise;
    };
    /**
     * Remove a member from one list or all lists.
     * @method
     * @name transmitsms#delete_from_list
     * @param {integer} list_id - ID of the list to remove from. If set to 0 (zero) the member will be removed from all lists.
     * @param {string} msisdn - Mobile number of the member
     * @param {string} Authorization - in format key secret
     * 
     */
    transmitsms.prototype.delete_from_list = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        var deferred = Q.defer();

        var domain = this.domain;
        var path = '/delete-from-list.json';

        var body;
        var queryParameters = {};
        var headers = {};
        var form = {};

        headers['Accept'] = ['application/json'];
        headers['Content-Type'] = ['application/x-www-form-urlencoded'];

        if (parameters['list_id'] !== undefined) {
            queryParameters['list_id'] = parameters['list_id'];
        }

        if (parameters['list_id'] === undefined) {
            deferred.reject(new Error('Missing required  parameter: list_id'));
            return deferred.promise;
        }

        if (parameters['msisdn'] !== undefined) {
            queryParameters['msisdn'] = parameters['msisdn'];
        }

        if (parameters['Authorization'] !== undefined) {
            headers['Authorization'] = parameters['Authorization'];
        }

        if (parameters['Authorization'] === undefined) {
            deferred.reject(new Error('Missing required  parameter: Authorization'));
            return deferred.promise;
        }

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters)
                .forEach(function(parameterName) {
                    var parameter = parameters.$queryParameters[parameterName];
                    queryParameters[parameterName] = parameter;
                });
        }

        this.request('POST', domain + path, parameters, body, headers, queryParameters, form, deferred);

        return deferred.promise;
    };
    /**
     * Get information about a list and its members.
     * @method
     * @name transmitsms#get_list
     * @param {integer} list_id - List ID
     * @param {string} members - Types of members to return. (active, inactive, all, none)
     * @param {integer} page - Page number, for pagination
     * @param {integer} max - Maximum results returned per page
     * @param {string} Authorization - API credentials
     * 
     */
    transmitsms.prototype.get_list = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        var deferred = Q.defer();

        var domain = this.domain;
        var path = '/get-list.json';

        var body;
        var queryParameters = {};
        var headers = {};
        var form = {};

        headers['Accept'] = ['application/json'];
        headers['Content-Type'] = ['application/x-www-form-urlencoded'];

        if (parameters['list_id'] !== undefined) {
            queryParameters['list_id'] = parameters['list_id'];
        }

        if (parameters['list_id'] === undefined) {
            deferred.reject(new Error('Missing required  parameter: list_id'));
            return deferred.promise;
        }

        if (parameters['members'] !== undefined) {
            queryParameters['members'] = parameters['members'];
        }

        if (parameters['page'] !== undefined) {
            queryParameters['page'] = parameters['page'];
        }

        if (parameters['max'] !== undefined) {
            queryParameters['max'] = parameters['max'];
        }

        if (parameters['Authorization'] !== undefined) {
            headers['Authorization'] = parameters['Authorization'];
        }

        if (parameters['Authorization'] === undefined) {
            deferred.reject(new Error('Missing required  parameter: Authorization'));
            return deferred.promise;
        }

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters)
                .forEach(function(parameterName) {
                    var parameter = parameters.$queryParameters[parameterName];
                    queryParameters[parameterName] = parameter;
                });
        }

        this.request('POST', domain + path, parameters, body, headers, queryParameters, form, deferred);

        return deferred.promise;
    };
    /**
     * Get the metadata of all your lists.
     * @method
     * @name transmitsms#get_lists
     * @param {integer} page - Page number, for pagination
     * @param {integer} max -   Maximum results returned per page
     * @param {string} Authorization - in format key secret
     * 
     */
    transmitsms.prototype.get_lists = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        var deferred = Q.defer();

        var domain = this.domain;
        var path = '/get-lists.json';

        var body;
        var queryParameters = {};
        var headers = {};
        var form = {};

        headers['Accept'] = ['application/json'];
        headers['Content-Type'] = ['application/x-www-form-urlencoded'];

        if (parameters['page'] !== undefined) {
            queryParameters['page'] = parameters['page'];
        }

        if (parameters['max'] !== undefined) {
            queryParameters['max'] = parameters['max'];
        }

        if (parameters['Authorization'] !== undefined) {
            headers['Authorization'] = parameters['Authorization'];
        }

        if (parameters['Authorization'] === undefined) {
            deferred.reject(new Error('Missing required  parameter: Authorization'));
            return deferred.promise;
        }

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters)
                .forEach(function(parameterName) {
                    var parameter = parameters.$queryParameters[parameterName];
                    queryParameters[parameterName] = parameter;
                });
        }

        this.request('POST', domain + path, parameters, body, headers, queryParameters, form, deferred);

        return deferred.promise;
    };
    /**
     * Delete a list and its members.
     * @method
     * @name transmitsms#remove_list
     * @param {integer} list_id - ID of the list to remove.
     * @param {string} Authorization - in format key secret
     * 
     */
    transmitsms.prototype.remove_list = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        var deferred = Q.defer();

        var domain = this.domain;
        var path = '/remove-list.json';

        var body;
        var queryParameters = {};
        var headers = {};
        var form = {};

        headers['Accept'] = ['application/json'];
        headers['Content-Type'] = ['application/x-www-form-urlencoded'];

        if (parameters['list_id'] !== undefined) {
            queryParameters['list_id'] = parameters['list_id'];
        }

        if (parameters['list_id'] === undefined) {
            deferred.reject(new Error('Missing required  parameter: list_id'));
            return deferred.promise;
        }

        if (parameters['Authorization'] !== undefined) {
            headers['Authorization'] = parameters['Authorization'];
        }

        if (parameters['Authorization'] === undefined) {
            deferred.reject(new Error('Missing required  parameter: Authorization'));
            return deferred.promise;
        }

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters)
                .forEach(function(parameterName) {
                    var parameter = parameters.$queryParameters[parameterName];
                    queryParameters[parameterName] = parameter;
                });
        }

        this.request('POST', domain + path, parameters, body, headers, queryParameters, form, deferred);

        return deferred.promise;
    };
    /**
     * Opt a user out of one list or all lists.
     * @method
     * @name transmitsms#optout_list_member
     * @param {integer} list_id - ID of the list to opt the user out of. Set this to 0 (zero) to opt out of all of your lists.
     * @param {string} msisdn - Mobile number of the member to opt out
     * @param {string} Authorization - in format key secret
     * 
     */
    transmitsms.prototype.optout_list_member = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        var deferred = Q.defer();

        var domain = this.domain;
        var path = '/optout-list-member.json';

        var body;
        var queryParameters = {};
        var headers = {};
        var form = {};

        headers['Accept'] = ['application/json'];
        headers['Content-Type'] = ['application/x-www-form-urlencoded'];

        if (parameters['list_id'] !== undefined) {
            queryParameters['list_id'] = parameters['list_id'];
        }

        if (parameters['list_id'] === undefined) {
            deferred.reject(new Error('Missing required  parameter: list_id'));
            return deferred.promise;
        }

        if (parameters['msisdn'] !== undefined) {
            queryParameters['msisdn'] = parameters['msisdn'];
        }

        if (parameters['Authorization'] !== undefined) {
            headers['Authorization'] = parameters['Authorization'];
        }

        if (parameters['Authorization'] === undefined) {
            deferred.reject(new Error('Missing required  parameter: Authorization'));
            return deferred.promise;
        }

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters)
                .forEach(function(parameterName) {
                    var parameter = parameters.$queryParameters[parameterName];
                    queryParameters[parameterName] = parameter;
                });
        }

        this.request('POST', domain + path, parameters, body, headers, queryParameters, form, deferred);

        return deferred.promise;
    };
    /**
     * Edit a member of a list.
     * @method
     * @name transmitsms#edit_list_member
     * @param {integer} list_id - ID of the list the member belongs to
     * @param {string} msisdn - Mobile number of the member to edit
     * @param {string} first_name - First name of the member
     * @param {string} last_name - Last name of the member
     * @param {string} field_1 - Custom field value 1
     * @param {string} field_2 - Custom field value 2
     * @param {string} field_3 - Custom field value 3
     * @param {string} field_4 - Custom field value 4
     * @param {string} field_5 - Custom field value 5
     * @param {string} field_6 - Custom field value 6
     * @param {string} field_7 - Custom field value 7
     * @param {string} field_8 - Custom field value 8
     * @param {string} field_9 - Custom field value 9
     * @param {string} field_10 - Custom field value 10
     * @param {string} Authorization - in format key secret
     * 
     */
    transmitsms.prototype.edit_list_member = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        var deferred = Q.defer();

        var domain = this.domain;
        var path = '/edit-list-member.json';

        var body;
        var queryParameters = {};
        var headers = {};
        var form = {};

        headers['Accept'] = ['application/json'];
        headers['Content-Type'] = ['application/x-www-form-urlencoded'];

        if (parameters['list_id'] !== undefined) {
            queryParameters['list_id'] = parameters['list_id'];
        }

        if (parameters['list_id'] === undefined) {
            deferred.reject(new Error('Missing required  parameter: list_id'));
            return deferred.promise;
        }

        if (parameters['msisdn'] !== undefined) {
            queryParameters['msisdn'] = parameters['msisdn'];
        }

        if (parameters['msisdn'] === undefined) {
            deferred.reject(new Error('Missing required  parameter: msisdn'));
            return deferred.promise;
        }

        if (parameters['first_name'] !== undefined) {
            queryParameters['first_name'] = parameters['first_name'];
        }

        if (parameters['last_name'] !== undefined) {
            queryParameters['last_name'] = parameters['last_name'];
        }

        if (parameters['field_1'] !== undefined) {
            queryParameters['field_1'] = parameters['field_1'];
        }

        if (parameters['field_2'] !== undefined) {
            queryParameters['field_2'] = parameters['field_2'];
        }

        if (parameters['field_3'] !== undefined) {
            queryParameters['field_3'] = parameters['field_3'];
        }

        if (parameters['field_4'] !== undefined) {
            queryParameters['field_4'] = parameters['field_4'];
        }

        if (parameters['field_5'] !== undefined) {
            queryParameters['field_5'] = parameters['field_5'];
        }

        if (parameters['field_6'] !== undefined) {
            queryParameters['field_6'] = parameters['field_6'];
        }

        if (parameters['field_7'] !== undefined) {
            queryParameters['field_7'] = parameters['field_7'];
        }

        if (parameters['field_8'] !== undefined) {
            queryParameters['field_8'] = parameters['field_8'];
        }

        if (parameters['field_9'] !== undefined) {
            queryParameters['field_9'] = parameters['field_9'];
        }

        if (parameters['field_10'] !== undefined) {
            queryParameters['field_10'] = parameters['field_10'];
        }

        if (parameters['Authorization'] !== undefined) {
            headers['Authorization'] = parameters['Authorization'];
        }

        if (parameters['Authorization'] === undefined) {
            deferred.reject(new Error('Missing required  parameter: Authorization'));
            return deferred.promise;
        }

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters)
                .forEach(function(parameterName) {
                    var parameter = parameters.$queryParameters[parameterName];
                    queryParameters[parameterName] = parameter;
                });
        }

        this.request('POST', domain + path, parameters, body, headers, queryParameters, form, deferred);

        return deferred.promise;
    };
    /**
     * Get detailed information about a response number you have leased.
     * @method
     * @name transmitsms#get_number
     * @param {integer} number - The virtual number to retrieve
     * @param {string} Authorization - API credentials
     * 
     */
    transmitsms.prototype.get_number = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        var deferred = Q.defer();

        var domain = this.domain;
        var path = '/get-number.json';

        var body;
        var queryParameters = {};
        var headers = {};
        var form = {};

        headers['Accept'] = ['application/json'];
        headers['Content-Type'] = ['application/x-www-form-urlencoded'];

        if (parameters['number'] !== undefined) {
            queryParameters['number'] = parameters['number'];
        }

        if (parameters['number'] === undefined) {
            deferred.reject(new Error('Missing required  parameter: number'));
            return deferred.promise;
        }

        if (parameters['Authorization'] !== undefined) {
            headers['Authorization'] = parameters['Authorization'];
        }

        if (parameters['Authorization'] === undefined) {
            deferred.reject(new Error('Missing required  parameter: Authorization'));
            return deferred.promise;
        }

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters)
                .forEach(function(parameterName) {
                    var parameter = parameters.$queryParameters[parameterName];
                    queryParameters[parameterName] = parameter;
                });
        }

        this.request('POST', domain + path, parameters, body, headers, queryParameters, form, deferred);

        return deferred.promise;
    };
    /**
     * Get a list of numbers either leased by you or available to be leased.
     * @method
     * @name transmitsms#get_numbers
     * @param {string} filter - Possible values are owned and available
     * @param {integer} page - Page number, for pagination
     * @param {integer} max -   Maximum results returned per page
     * @param {string} Authorization - in format key secret
     * 
     */
    transmitsms.prototype.get_numbers = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        var deferred = Q.defer();

        var domain = this.domain;
        var path = '/get-numbers.json';

        var body;
        var queryParameters = {};
        var headers = {};
        var form = {};

        headers['Accept'] = ['application/json'];
        headers['Content-Type'] = ['application/x-www-form-urlencoded'];

        if (parameters['filter'] !== undefined) {
            queryParameters['filter'] = parameters['filter'];
        }

        if (parameters['page'] !== undefined) {
            queryParameters['page'] = parameters['page'];
        }

        if (parameters['max'] !== undefined) {
            queryParameters['max'] = parameters['max'];
        }

        if (parameters['Authorization'] !== undefined) {
            headers['Authorization'] = parameters['Authorization'];
        }

        if (parameters['Authorization'] === undefined) {
            deferred.reject(new Error('Missing required  parameter: Authorization'));
            return deferred.promise;
        }

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters)
                .forEach(function(parameterName) {
                    var parameter = parameters.$queryParameters[parameterName];
                    queryParameters[parameterName] = parameter;
                });
        }

        this.request('POST', domain + path, parameters, body, headers, queryParameters, form, deferred);

        return deferred.promise;
    };
    /**
     * Lease a dedicated virtual number
     * @method
     * @name transmitsms#lease_number
     * @param {string} number - The virtual number to lease. Omit this field to be given a random number. Use get-numbers to find out which numbers are currently available.
     * @param {string} Authorization - in format key secret
     * 
     */
    transmitsms.prototype.lease_number = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        var deferred = Q.defer();

        var domain = this.domain;
        var path = '/lease-number.json';

        var body;
        var queryParameters = {};
        var headers = {};
        var form = {};

        headers['Accept'] = ['application/json'];
        headers['Content-Type'] = ['application/x-www-form-urlencoded'];

        if (parameters['number'] !== undefined) {
            queryParameters['number'] = parameters['number'];
        }

        if (parameters['Authorization'] !== undefined) {
            headers['Authorization'] = parameters['Authorization'];
        }

        if (parameters['Authorization'] === undefined) {
            deferred.reject(new Error('Missing required  parameter: Authorization'));
            return deferred.promise;
        }

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters)
                .forEach(function(parameterName) {
                    var parameter = parameters.$queryParameters[parameterName];
                    queryParameters[parameterName] = parameter;
                });
        }

        this.request('POST', domain + path, parameters, body, headers, queryParameters, form, deferred);

        return deferred.promise;
    };
        /**
     * 
     * @method
     * @name transmitsms#get_sms_stats
     * @param {integer} message_id - Message ID's are made up of digits
     * @param {string} Authorization - in format key secret
     * 
     */
    transmitsms.prototype.get_sms_stats = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        var deferred = Q.defer();

        var domain = this.domain;
        var path = '/get-sms-stats.json';

        var body;
        var queryParameters = {};
        var headers = {};
        var form = {};

        if (parameters['message_id'] !== undefined) {
            queryParameters['message_id'] = parameters['message_id'];
        }

        if (parameters['Authorization'] !== undefined) {
            headers['Authorization'] = parameters['Authorization'];
        }

        if (parameters['Authorization'] === undefined) {
            deferred.reject(new Error('Missing required  parameter: Authorization'));
            return deferred.promise;
        }

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters)
                .forEach(function(parameterName) {
                    var parameter = parameters.$queryParameters[parameterName];
                    queryParameters[parameterName] = parameter;
                });
        }

        var req = {
            method: 'GET',
            uri: domain + path,
            qs: queryParameters,
            headers: headers,
            body: body
        };
        if (Object.keys(form).length > 0) {
            req.form = form;
        }
        if (typeof(body) === 'object') {
            req.json = true;
        }
        request(req, function(error, response, body) {
            if (error) {
                deferred.reject(error);
            } else {
                if (/^application\/(.*\\+)?json/.test(response.headers['content-type'])) {
                    try {
                        body = JSON.parse(body);
                    } catch (e) {

                    }
                }
                if (response.statusCode >= 200 && response.statusCode <= 299) {
                    deferred.resolve({
                        response: response,
                        body: body
                    });
                } else {
                    deferred.reject({
                        response: response,
                        body: body
                    });
                }
            }
        });

        return deferred.promise;
    };

    return transmitsms;
})();

exports.transmitsms = transmitsms;